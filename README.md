# Portfolio #

## Dependencies:

* React
* jQuery
* Snap.svg
* Scrollspy.js
* TweenMax
* Contentful SDK
  
  
## Notes:

  
* This portfolio consumes an API provided by [Contentful](https://www.contentful.com/) through their provided SDK - hence all XHR activity is abstracted by their proprietary methods.

* Views are defined in React - originally both dynamic sections of the site used the same design, which has diverged, and both are now distinct. This leads to a few quirks in the code's architecture, namely the reliance on the particular 'instance' of the 'app' as a pseudo-namespace to allow them to be differentiated. Ideally, these two different view-types would be refactored to distinct 'apps'.

* Browser support matches [Google Apps](https://support.google.com/a/answer/33864), with one instance of a -clip-path element being hidden in Firefox owing to lack of support and the element's non-essential nature.

*  Styles are arranged according to [ITCSS](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture/) structure, written in SASS, and compiled through the included Gulp watch/build task.

* Some key Javascript dependencies (Babel etc) are provided through [JSPM](http://jspm.io/) for development, which in production is all bundled into a single self-executing file.

* SVG animation is handled through [Snap.svg](http://snapsvg.io/).

* Gooey navigation marker is handled through [TweenMax](https://greensock.com/), with events triggered by [Scrollspy](http://v4-alpha.getbootstrap.com/components/scrollspy/).

## Setup:

All npm/jspm/bower dependencies are included in the repo, as well as the gulpfile, meaning that the repo should be executable by cloning down, installing local gulp via npm, and running 
```
#!cli

gulp serve
```
in the repo.

The production ready files are also provided in the /dist folder.