export const uniqueId = (function() {
  var count = 0;
  return function() {
    return 'id' + count++;
  }
})();
