// --------------------------------------------------------------------------
// Module imports
// --------------------------------------------------------------------------

// React import
import React from 'react';

// ReactDOM import
import ReactDOM from 'react-dom';

// showdown import - markdown parsing
import showdown from 'showdown';

// import unique ID generating function
import {uniqueId} from 'scripts/helpers/uniqueId';

// React classNames module - classSet deprecated
import classNames from'classnames';

// React CSS transition module - lifecycle classes applied around DOM rewrites (eg enter/leave)
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

// --------------------------------------------------------------------------
// Variables
// --------------------------------------------------------------------------

// Instantiate showdown
let converter = new showdown.Converter();

// Instantiate Contentful SDK
let content = contentful.createClient({
  space: '8qxvw58ed4dv',
  accessToken: '21e220bf45b7ba949a3def729ec08db8d5d0410cd0044ae0a5b2167c885eb2e2'
});

// Create empty state object
const state = {
}

// Empty selection arrays and stored paths to animate with Snap
let pagerTriangle = [],
pagerRectangle = [],
shapes = {
  compoundPaths : ["M9.8,0.9L10.9,8L0.1,5.4L9.8,0.9z M1.2,5.8l9.8,2.4l-6,1.9L1.2,5.8z", "M9.1,8.2l-7.1,2.2L0.6,1.9L9.1,8.2z M10.4,1.3L3.7,3.8l5.4,4L10.4,1.3z", "M10.6,4.5L6.5,11L0.3,6.4L10.6,4.5z M8.9,4.6l-5.4-3L0.3,6.2L8.9,4.6z", "M10.3,9H0.2l5.2-8.2L10.3,9z M8,4.7l2.8-4.4H5.5L8,4.7z", "M8.8,5.9L4.6,11L0,3.2L8.8,5.9z M0.1,3.1l8.4,2.6L11,2.5L0.1,3.1z", "M11,0.4l-0.9,7.1L1.8,0.4H11z M10.1,8.2L4,2.8L0.2,11L10.1,8.2z", "M5.5,11l2.7-4.7L11,1.4H0L5.5,11z", "M5.5,1.4L2.81,6.1,0,11H11Z"],
  simplePaths : ["M9.8,1.9L11,9.1L0.1,6.4L9.8,1.9z", "M11,8.1L1.7,11L0,0L11,8.1z", "M11,1.9L6.6,8.8L0,4L11,1.9z", "M11.1,10H0L5.8,1L11.1,10z", "M11,4.5l-5.2,6.3L0.1,1.2L11,4.5z", "M10.8,0.9L9.8,9.3L0.1,0.9H10.8z", "M5.5,11l2.7-4.7L11,1.4H0L5.5,11z", "M5.5,1.4L2.81,6.1,0,11H11Z"]
};


const actions = {
  toggleNext: function(instance) {
    // Get current index
    let current = state[instance].current;
    // Set next index to current + 1
    let next = current + 1;
    // If next index would be higher than numbers of items, next loops back to 0
    if (next > state[instance].length - 1) {
      next = 0;
    }
    // Set new current index
    state[instance].current = next;
    // Re-render state
    render(state)
  },
  togglePrev: function(instance) {
    // Get current index
    let current = state[instance].current;
    // Set previous index to current - 1
    let prev = current - 1;
    // If previous index would be negative, previous loops back to last item
    if (prev < 0) {
      prev = state[instance].length - 1;
    }
    // Set new current index
    state[instance].current = prev;
    // Re-render state
    render(state);
  },
  toggleSlide: function(id, shape, instance) {
    // Map available items by title
    let index = state[instance].map(function (el) {
      return (
        el.fields.title
      );
    });
    // Find item to make active by index of title in mapped array
    let currentIndex = index.indexOf(id);
    // If no item open, set item index to current, set instance to open and re-render
    if (state[instance].open == false) {
      state[instance].current = currentIndex;
      state[instance].open = true;
    }
    // If a different item is open, close it and set the newly selected item to current, re-render
    else if (state[instance].open == true && state[instance].current != currentIndex) {
      actions.closeProjects(state[instance][state[instance].current].fields.title , shapes.compoundPaths[state[instance].current], instance, state[instance].current);
      state[instance].current = currentIndex;
      state[instance].open = true;
    }
    // Otherwise close the item - signifies closing an open item
    else {
      actions.closeProjects(id, shape, instance);
    }

    render(state);

    // Select current item and animate it to a rectangle
    pagerRectangle = Snap.selectAll('.slide .pager-item path')[currentIndex];
    animateToRectangle();

  },
  closeProjects: function(id, shape, instance, targetId) {
    // Map out indexes by title
    let index = state[instance].map(function (el) {
      return (
        el.fields.title
      );
    });
    let currentIndex = index.indexOf(id);

    // If called with a target id, select it to animate back to triangle
    if (targetId) {
        pagerTriangle = Snap.selectAll('.slide .pager-item path')[targetId];
    }
    // Otherwise, set to closed, eliminate current value and animate current item back to triangle
    else {
      state[instance].open = false;
      state[instance].current = false;
      pagerTriangle = Snap.selectAll('.slide .pager-item path')[currentIndex];
    }

    render(state);

    // If a shape is supplied, use setTimeout to jimmy around DOM rerender, and call triangle animation function
    if (shape) {
      setTimeout(function(){
        animateToTriangle(shape);
      }, 50);
    }
  },
  setActive: function(id, instance) {
    // Simply set index of selected item to active and rerender
    let index = state[instance].map(function(el){
      return (
        el.fields.pieceTitle
      );
    });
    let currentIndex = index.indexOf(id);
    state[instance].current = currentIndex;
    state[instance].open = true;
    render(state);
  }
}

// --------------------------------------------------------------------------
// Querying Contentful API for associated data
// --------------------------------------------------------------------------

getData('projects').then(function(response) {
  render(state);
}, function(error) {
  console.log(error);
});
getData('art').then(function(response) {
  render(state);
}, function(error) {
  console.log(error);
});

// --------------------------------------------------------------------------
// Portfolio Components
// --------------------------------------------------------------------------

// Parent portfolio component
class Portfolio extends React.Component {
  render() {
    return (
      <div className="portfolio">
      <Projects data={this.props.data} instance={this.props.instance} />
      </div>
    );
  }
  componentDidMount() {
    let instanceProp = this.props.instance;
    state[instanceProp].current = false;
    state[instanceProp].open = false;
  }
}



// Individual project component, manifests all exposed data pertaining to individual project
class Project extends React.Component {
  render() {
    let classes = classNames({
      'slide': true,
      'slide--active': this.props.active
    }),
    expandedInfo = <div className="detail" dangerouslySetInnerHTML={{__html: converter.makeHtml(this.props.text)}}></div>;
    return (
      <div className={classes}>
        <div className="project-image">
          <Clip shape={this.props.shape} imagePath={this.props.imagePath} instance={this.props.instance} />
        </div>
        <div className="project-info">
          <h2>{this.props.title}</h2>
          <h3>Url: <a href={"http://href.li/?" + this.props.subtitle} target="_blank">{this.props.subtitle}</a></h3>
          {this.props.tags ? <Tags tags={this.props.tags} /> : null}
          <ReactCSSTransitionGroup transitionName="detail" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
          {this.props.active ? expandedInfo : null}
          </ReactCSSTransitionGroup>
          <ExpandButton id={this.props.id} shape={this.props.shape} instance={this.props.instance} active={this.props.active} />
        </div>
      </div>
    );
  }
}

// Parent portfolio component - maps data as props to children
class Projects extends React.Component {
  render() {
    // Hamfisted storing instance property in letiable to pass into map
    let instanceProp = this.props.instance,
    activeNode = state[instanceProp].current;
    let paginationNodes = this.props.data.map(function(paginationNode, index) {
      let isActive = activeNode === index;
      return (
        <Project id={paginationNode.fields.title} shape={shapes.compoundPaths[index]} active={isActive} key={paginationNode.fields.title} title={paginationNode.fields.title} subtitle={paginationNode.fields.liveUrl} text={paginationNode.fields.rundown} imagePath={paginationNode.fields.screenshot.fields.file.url} tags={paginationNode.fields.tags} instance={instanceProp} />
      );
    });
    return (
      <div className="pagination">
      {paginationNodes}
      </div>
    );
  }
}

// Tags component for Portfolio Item - maps tags in array to list items
class Tags extends React.Component {
  render() {
    let tags = this.props.tags.map(function(tag, index) {
      return (
        <li key={tag} className="tag">{tag}</li>
      );
    });
    return (
      <ul className="tag-list">
      {tags}
      </ul>
    );
  }
}

// Button to expand details of Portfolio item - changes text if active
class ExpandButton extends React.Component {
  render() {
    let classes = classNames({
      "expand-button" : true,
      "expand-button--active" : this.props.active
    });
    return (
      <div>
        <button onClick={this.toggleSlide.bind(this)} className={classes}>
          {this.props.active ? "Close" : "View Details" }
          <svg viewBox="0 0 11 11">
            <use xlinkHref="images/arrow.svg#arrow"></use>
          </svg>
        </button>
      </div>
    );
  }
  toggleSlide() {
      actions.toggleSlide(this.props.id, this.props.shape, this.props.instance);
  }
}

// --------------------------------------------------------------------------
 // Art Gallery Components
// --------------------------------------------------------------------------

// Gallery parent component - displays all thumnbnails, changes structure when items opened
class Gallery extends React.Component {
  render() {
    // Hamfisted storing instance property in letiable to pass into map
    let instanceProp = this.props.instance,
    activeNode = state[instanceProp].current,
    classes = classNames({
      "gallery-container" : true,
      "gallery-container--active" : state[instanceProp].open
    });

    let paginationNodes = this.props.data.map(function(paginationNode, index) {
      let isActive = activeNode === index;
      return (
        <GalleryThumbnail id={paginationNode.fields.pieceTitle} active={isActive} key={paginationNode.fields.pieceTitle} title={paginationNode.fields.pieceTitle} shape={shapes.simplePaths[index]} fullImage={paginationNode.fields.fullImage.fields.file.url} thumbnailImage={paginationNode.fields.thumbnail.fields.file.url} media={paginationNode.fields.media} instance={instanceProp} />
      );
    });

    let previousNode = activeNode == 0 ? paginationNodes.length - 1 : activeNode - 1,
    nextNode = activeNode == paginationNodes.length - 1 ? 0 : activeNode + 1;

    if (state[instanceProp].open) {
      var galleryView = <div className="gallery-view" key="gallery-active" ><Close instance={instanceProp} /><div className="prev"><ControlPrev instance={instanceProp} referenceNode={paginationNodes[previousNode].props} /></div><div className="featured-item">{paginationNodes[activeNode]}</div><div className="next"><ControlNext instance={instanceProp} referenceNode={paginationNodes[nextNode].props}  /></div></div>;
    }
    else {
      var galleryView = <div className="pagination" key="gallery-inactive">{paginationNodes}</div>;
    }

    return (
      <div className={classes}>
        <ReactCSSTransitionGroup transitionName="artOverlay" transitionEnterTimeout={800} transitionLeaveTimeout={100}>
          {galleryView}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
  componentDidMount() {
    state[this.props.instance].open = false;
  }
}

// Individual Gallery piece - displays full details if selected
class GalleryThumbnail extends React.Component {
  render() {
    if(this.props.active) {
      var imageView =  <div><img src={this.props.fullImage} alt="placeholder" /><div className="gallery__info"><h2>{this.props.title}</h2><p>{this.props.media}</p><div><a href={this.props.fullImage} className="expand-button" target="_blank">View in New Tab <svg viewBox="0 0 11 11"><use xlinkHref="images/arrow.svg#arrow"></use></svg></a></div></div></div>;
    }
    else {
      var imageView = <Clip imagePath={this.props.thumbnailImage} shape={this.props.shape} />;
    }
    return (
      <div className="gallery__thumbnail" onClick={this.setActive.bind(this)}>
        {imageView}
      </div>
    );
  }
  setActive() {
    actions.setActive(this.props.id, this.props.instance);
  }
}

// Previous button for Gallery View - pulls Clip component of adjacent item
class ControlPrev extends React.Component {
  togglePrev() {
    actions.togglePrev(this.props.instance);
  }
  render() {
    return (
      <div className="toggle-prev toggle" onClick={this.togglePrev.bind(this)}>
        <Clip imagePath={this.props.referenceNode.thumbnailImage} shape={this.props.referenceNode.shape} />
        <span>&lt;</span>
      </div>
    );
  }
}

// Next button for Gallery View - pulls Clip component of adjacent item
class ControlNext extends React.Component {
  toggleNext() {
    actions.toggleNext(this.props.instance);
  }
  render() {
    return (
      <div className="toggle-next toggle" onClick={this.toggleNext.bind(this)}>
        <Clip imagePath={this.props.referenceNode.thumbnailImage} shape={this.props.referenceNode.shape} />
        <span>&gt;</span>
      </div>
    );
  }
}

// Close button for Art gallery view - button which calls closeProjects function
class Close extends React.Component {
  closeProjects() {
    actions.closeProjects(false, false, this.props.instance);
  }
  render() {
    return (
      <button className="close-button" onClick={this.closeProjects.bind(this)}>X</button>
    );
  }
}


// --------------------------------------------------------------------------
 // Shared Components
// --------------------------------------------------------------------------

// SVG shape containing Raster image background - used to animate as a pseudo clipping mask with Snap.svg
class Clip extends React.Component {
  render() {
    let id = uniqueId();
    return (
      <svg viewBox="0 0 11 11" className="pager-item">
        <defs>
          <pattern id={id} x="10" y="10" width="1" height="1" patternUnits="objectBoundingBox">
            <image xlinkHref={this.props.imagePath} x="0" y="-3.5" width="12" height="17" />
          </pattern>
        </defs>
        <path fill={"url(#" + id + ")"} d={this.props.shape} />
      </svg>
    );
  }
}

// --------------------------------------------------------------------------
// Functions
// --------------------------------------------------------------------------

// State rendering function
// Passes state object as param
function render(state) {
  ReactDOM.render(
    <Portfolio data={state.projects} instance="projects"/>,
    document.getElementById('projects-app')
  );
  ReactDOM.render(
    <Gallery data={state.art} instance="art"/>,
    document.getElementById('art-app')
  );
}

// Function to animate SVG objects to rectangle ratio
function animateToRectangle() {
  let squarePoints = "M0,11h11V5.5V0H0v5.5V11z";
  pagerRectangle.animate({ d: squarePoints }, 1000, mina.easeinout);
}
// Function to animate SVG objects to triangle ratio
function animateToTriangle(shape) {
  pagerTriangle.animate({ d: shape }, 1000, mina.easeinout);
}

// XHR via Contentful SDK to retrieve data structured in contentful
function getData(dataset) {

  return new Promise(function(resolve, reject) {

      state[dataset] = new Array();

      content.getEntries({
        'content_type': (dataset == "projects") ? 'portfolioItem' : 'artPiece',
        order: 'sys.createdAt'
      }).then(function (entries) {
        state[dataset].push.apply(state[dataset], entries.items);
      }).then(function() {
        if(state[dataset].length > 0) {
          resolve(true);
        }
        else {
          reject(Error('Failed to connect and retrieve' + dataset));
        }
      });
    });
  }
