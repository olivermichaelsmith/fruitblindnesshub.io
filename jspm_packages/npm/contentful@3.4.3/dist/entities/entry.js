/* */ 
'use strict';
Object.defineProperty(exports, "__esModule", {value: true});
exports.wrapEntry = wrapEntry;
exports.wrapEntryCollection = wrapEntryCollection;
var _cloneDeep = require('lodash/cloneDeep');
var _cloneDeep2 = _interopRequireDefault(_cloneDeep);
var _uniq = require('lodash/uniq');
var _uniq2 = _interopRequireDefault(_uniq);
var _toPlainObject = require('contentful-sdk-core/mixins/to-plain-object');
var _toPlainObject2 = _interopRequireDefault(_toPlainObject);
var _freezeSys = require('contentful-sdk-core/freeze-sys');
var _freezeSys2 = _interopRequireDefault(_freezeSys);
var _linkGetters = require('../mixins/link-getters');
var _linkGetters2 = _interopRequireDefault(_linkGetters);
var _stringifySafe = require('../mixins/stringify-safe');
var _stringifySafe2 = _interopRequireDefault(_stringifySafe);
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {default: obj};
}
function wrapEntry(data) {
  return (0, _freezeSys2.default)((0, _toPlainObject2.default)((0, _cloneDeep2.default)(data)));
}
function wrapEntryCollection(data, resolveLinks, resolveForAllLocales) {
  var wrappedData = (0, _stringifySafe2.default)((0, _toPlainObject2.default)((0, _cloneDeep2.default)(data)));
  if (resolveLinks) {
    var includes = prepareIncludes(wrappedData.includes, wrappedData.items);
    (0, _linkGetters2.default)(wrappedData.items, includes, resolveForAllLocales);
  }
  return (0, _freezeSys2.default)(wrappedData);
}
function prepareIncludes() {
  var includes = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
  var items = arguments[1];
  includes.Entry = includes.Entry || [];
  includes.Entry = (0, _uniq2.default)(includes.Entry.concat((0, _cloneDeep2.default)(items)));
  return includes;
}
