/* */ 
'use strict';
Object.defineProperty(exports, "__esModule", {value: true});
exports.default = createClient;
var _defaults = require('lodash/defaults');
var _defaults2 = _interopRequireDefault(_defaults);
var _assign = require('lodash/assign');
var _assign2 = _interopRequireDefault(_assign);
var _cloneDeep = require('lodash/cloneDeep');
var _cloneDeep2 = _interopRequireDefault(_cloneDeep);
var _version = require('../version');
var _version2 = _interopRequireDefault(_version);
var _createHttpClient = require('contentful-sdk-core/create-http-client');
var _createHttpClient2 = _interopRequireDefault(_createHttpClient);
var _wrapHttpClient = require('contentful-sdk-core/wrap-http-client');
var _wrapHttpClient2 = _interopRequireDefault(_wrapHttpClient);
var _createContentfulApi = require('./create-contentful-api');
var _createContentfulApi2 = _interopRequireDefault(_createContentfulApi);
var _createLinkResolver = require('./create-link-resolver');
var _createLinkResolver2 = _interopRequireDefault(_createLinkResolver);
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {default: obj};
}
function createClient(axios, params) {
  params = (0, _defaults2.default)((0, _cloneDeep2.default)(params), {
    rateLimit: 9,
    rateLimitPeriod: 1000,
    maxRetries: 5,
    retryOnTooManyRequests: true
  });
  if (!params.accessToken) {
    throw new TypeError('Expected parameter accessToken');
  }
  if (!params.space) {
    throw new TypeError('Expected parameter space');
  }
  var resolveLinks = !!('resolveLinks' in params ? params.resolveLinks : true);
  var shouldLinksResolve = (0, _createLinkResolver2.default)(resolveLinks);
  params.defaultHostname = 'cdn.contentful.com';
  params.headers = (0, _assign2.default)(params.headers, {
    'Content-Type': 'application/vnd.contentful.delivery.v1+json',
    'X-Contentful-User-Agent': 'contentful.js/' + _version2.default
  });
  var http = (0, _wrapHttpClient2.default)((0, _createHttpClient2.default)(axios, params), {
    concurrency: params.rateLimit,
    delay: params.rateLimitPeriod,
    maxRetries: params.maxRetries,
    retryOnTooManyRequests: params.retryOnTooManyRequests
  });
  return (0, _createContentfulApi2.default)({
    http: http,
    shouldLinksResolve: shouldLinksResolve
  });
}
