/* */ 
'use strict';
Object.defineProperty(exports, "__esModule", {value: true});
exports.default = createContentfulApi;
var _createRequestConfig = require('contentful-sdk-core/create-request-config');
var _createRequestConfig2 = _interopRequireDefault(_createRequestConfig);
var _entities = require('./entities/index');
var _entities2 = _interopRequireDefault(_entities);
var _pagedSync = require('./paged-sync');
var _pagedSync2 = _interopRequireDefault(_pagedSync);
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {default: obj};
}
function createContentfulApi(_ref) {
  var http = _ref.http;
  var shouldLinksResolve = _ref.shouldLinksResolve;
  var wrapSpace = _entities2.default.space.wrapSpace;
  var _entities$contentType = _entities2.default.contentType;
  var wrapContentType = _entities$contentType.wrapContentType;
  var wrapContentTypeCollection = _entities$contentType.wrapContentTypeCollection;
  var _entities$entry = _entities2.default.entry;
  var wrapEntry = _entities$entry.wrapEntry;
  var wrapEntryCollection = _entities$entry.wrapEntryCollection;
  var _entities$asset = _entities2.default.asset;
  var wrapAsset = _entities$asset.wrapAsset;
  var wrapAssetCollection = _entities$asset.wrapAssetCollection;
  function errorHandler(error) {
    if (error.data) {
      throw error.data;
    }
    throw error;
  }
  function getSpace() {
    return http.get('').then(function(response) {
      return wrapSpace(response.data);
    }, errorHandler);
  }
  function getContentType(id) {
    return http.get('content_types/' + id).then(function(response) {
      return wrapContentType(response.data);
    }, errorHandler);
  }
  function getContentTypes() {
    var query = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    return http.get('content_types', (0, _createRequestConfig2.default)({query: query})).then(function(response) {
      return wrapContentTypeCollection(response.data);
    }, errorHandler);
  }
  function getEntry(id) {
    var query = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    return http.get('entries/' + id, (0, _createRequestConfig2.default)({query: query})).then(function(response) {
      return wrapEntry(response.data);
    }, errorHandler);
  }
  function getEntries() {
    var query = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var resolveLinks = shouldLinksResolve(query);
    var resolveForAllLocales = query.locale && query.locale === '*';
    return http.get('entries', (0, _createRequestConfig2.default)({query: query})).then(function(response) {
      return wrapEntryCollection(response.data, resolveLinks, resolveForAllLocales);
    }, errorHandler);
  }
  function getAsset(id) {
    var query = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    return http.get('assets/' + id, (0, _createRequestConfig2.default)({query: query})).then(function(response) {
      return wrapAsset(response.data);
    }, errorHandler);
  }
  function getAssets() {
    var query = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    return http.get('assets', (0, _createRequestConfig2.default)({query: query})).then(function(response) {
      return wrapAssetCollection(response.data);
    }, errorHandler);
  }
  function sync() {
    var query = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];
    var resolveLinks = shouldLinksResolve(query);
    return (0, _pagedSync2.default)(http, query, resolveLinks);
  }
  return {
    getSpace: getSpace,
    getContentType: getContentType,
    getContentTypes: getContentTypes,
    getEntry: getEntry,
    getEntries: getEntries,
    getAsset: getAsset,
    getAssets: getAssets,
    sync: sync
  };
}
