/* */ 
'use strict';
Object.defineProperty(exports, "__esModule", {value: true});
exports.default = pagedSync;
var _filter = require('lodash/filter');
var _filter2 = _interopRequireDefault(_filter);
var _cloneDeep = require('lodash/cloneDeep');
var _cloneDeep2 = _interopRequireDefault(_cloneDeep);
var _createRequestConfig = require('contentful-sdk-core/create-request-config');
var _createRequestConfig2 = _interopRequireDefault(_createRequestConfig);
var _freezeSys = require('contentful-sdk-core/freeze-sys');
var _freezeSys2 = _interopRequireDefault(_freezeSys);
var _linkGetters = require('./mixins/link-getters');
var _linkGetters2 = _interopRequireDefault(_linkGetters);
var _stringifySafe = require('./mixins/stringify-safe');
var _stringifySafe2 = _interopRequireDefault(_stringifySafe);
var _toPlainObject = require('contentful-sdk-core/mixins/to-plain-object');
var _toPlainObject2 = _interopRequireDefault(_toPlainObject);
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {default: obj};
}
function pagedSync(http, query, resolveLinks) {
  if (!query || !query.initial && !query.nextSyncToken) {
    throw new Error('Please provide one of `initial` or `nextSyncToken` parameters for syncing');
  }
  if (query && query.content_type && !query.type) {
    query.type = 'Entry';
  } else if (query && query.content_type && query.type && query.type !== 'Entry') {
    throw new Error('When using the `content_type` filter your `type` parameter cannot be different from `Entry`.');
  }
  if (query.nextSyncToken) {
    query.sync_token = query.nextSyncToken;
    delete query.initial;
    delete query.nextSyncToken;
  }
  return getSyncPage(http, [], query).then(function(response) {
    if (resolveLinks) {
      (0, _linkGetters2.default)(response.items, mapIncludeItems((0, _cloneDeep2.default)(response.items)));
    }
    var mappedResponseItems = mapResponseItems(response.items);
    mappedResponseItems.nextSyncToken = response.nextSyncToken;
    return (0, _freezeSys2.default)((0, _stringifySafe2.default)((0, _toPlainObject2.default)(mappedResponseItems)));
  }, function(error) {
    throw error.data;
  });
}
function mapResponseItems(items) {
  return {
    entries: (0, _filter2.default)(items, ['sys.type', 'Entry']),
    assets: (0, _filter2.default)(items, ['sys.type', 'Asset']),
    deletedEntries: (0, _filter2.default)(items, ['sys.type', 'DeletedEntry']),
    deletedAssets: (0, _filter2.default)(items, ['sys.type', 'DeletedAsset'])
  };
}
function mapIncludeItems(items) {
  return {
    Entry: (0, _filter2.default)(items, ['sys.type', 'Entry']),
    Asset: (0, _filter2.default)(items, ['sys.type', 'Asset'])
  };
}
function getSyncPage(http, items, query) {
  return http.get('sync', (0, _createRequestConfig2.default)({query: query})).then(function(response) {
    var data = response.data;
    items = items.concat(data.items);
    if (data.nextPageUrl) {
      delete query.initial;
      query.sync_token = getToken(data.nextPageUrl);
      return getSyncPage(http, items, query);
    } else if (data.nextSyncUrl) {
      return {
        items: items,
        nextSyncToken: getToken(data.nextSyncUrl)
      };
    }
  });
}
function getToken(url) {
  var urlParts = url.split('?');
  return urlParts.length > 0 ? urlParts[1].replace('sync_token=', '') : '';
}
