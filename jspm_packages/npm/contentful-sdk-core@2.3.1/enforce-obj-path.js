/* */ 
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

exports.default = enforceObjPath;

var _has = require('lodash/has');

var _has2 = _interopRequireDefault(_has);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function enforceObjPath(obj, path) {
  if (!(0, _has2.default)(obj, path)) {
    var err = new Error();
    err.name = 'PropertyMissing';
    err.message = 'Required property ' + path + ' missing from:\n\n' + (0, _stringify2.default)(obj) + '\n\n';
    throw err;
  }
  return true;
}