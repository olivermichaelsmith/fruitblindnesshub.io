/* */ 
'use strict';
Object.defineProperty(exports, "__esModule", {value: true});
var _promise = require('babel-runtime/core-js/promise');
var _promise2 = _interopRequireDefault(_promise);
exports.default = rateLimit;
var _promisedWait = require('./promised-wait');
var _promisedWait2 = _interopRequireDefault(_promisedWait);
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {default: obj};
}
function rateLimit(fn, concurrency, delay) {
  concurrency = positiveInteger('concurrency', concurrency);
  delay = positiveInteger('delay', delay);
  var callQueue = [];
  var inFlight = 0;
  function shift() {
    if (inFlight >= concurrency) {
      return;
    }
    var start = new Date().getTime();
    if (callQueue.length) {
      var call = callQueue.shift();
      inFlight++;
      var result = void 0;
      try {
        var tmp = _promise2.default.resolve(fn.apply(call.self, call.args));
        call.resolve(tmp);
        result = tmp.catch(function() {});
      } catch (err) {
        call.reject(err);
        result = _promise2.default.resolve();
      }
      result.then(maybeWait).then(goToNextCall);
    }
    function maybeWait() {
      var duration = start - new Date().getTime();
      if (duration < delay) {
        return (0, _promisedWait2.default)(delay - duration);
      }
    }
  }
  function goToNextCall() {
    inFlight--;
    shift();
  }
  return function() {
    var self = this;
    var args = Array.prototype.slice.call(arguments);
    return new _promise2.default(function(resolve, reject) {
      callQueue.push({
        reject: reject,
        resolve: resolve,
        self: self,
        args: args
      });
      shift();
    });
  };
}
function positiveInteger(name, value) {
  value = parseInt(value, 10);
  if (isNaN(value) || value < 1) {
    throw new TypeError(name + ' must be a positive integer');
  }
  return value;
}
