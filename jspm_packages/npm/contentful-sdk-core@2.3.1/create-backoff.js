/* */ 
'use strict';
Object.defineProperty(exports, "__esModule", {value: true});
var _promise = require('babel-runtime/core-js/promise');
var _promise2 = _interopRequireDefault(_promise);
exports.default = createBackoff;
var _promisedWait = require('./promised-wait');
var _promisedWait2 = _interopRequireDefault(_promisedWait);
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {default: obj};
}
function createBackoff(maxRetries) {
  var attempt = 0;
  return function maybeRetry(error, retry) {
    if (attempt < maxRetries) {
      return (0, _promisedWait2.default)(Math.pow(2, attempt++) * 1000).then(retry);
    } else {
      return _promise2.default.reject(error);
    }
  };
}
