/* */ 
(function(process) {
  'use strict';
  Object.defineProperty(exports, "__esModule", {value: true});
  var _slicedToArray2 = require('babel-runtime/helpers/slicedToArray');
  var _slicedToArray3 = _interopRequireDefault(_slicedToArray2);
  exports.default = createHttpClient;
  var _qs = require('qs');
  var _qs2 = _interopRequireDefault(_qs);
  var _cloneDeep = require('lodash/cloneDeep');
  var _cloneDeep2 = _interopRequireDefault(_cloneDeep);
  var _assign = require('lodash/assign');
  var _assign2 = _interopRequireDefault(_assign);
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {default: obj};
  }
  function createHttpClient(axios, httpClientParams) {
    var space = httpClientParams.space;
    var accessToken = httpClientParams.accessToken;
    var insecure = httpClientParams.insecure;
    var host = httpClientParams.host;
    var defaultHostname = httpClientParams.defaultHostname;
    var agent = httpClientParams.agent;
    var headers = httpClientParams.headers;
    var _ref = host && host.split(':') || [];
    var _ref2 = (0, _slicedToArray3.default)(_ref, 2);
    var hostname = _ref2[0];
    var port = _ref2[1];
    hostname = hostname || defaultHostname;
    port = port || (insecure ? 80 : 443);
    var baseURL = (insecure ? 'http' : 'https') + '://' + hostname + ':' + port + '/spaces/';
    if (space) {
      baseURL += space + '/';
    }
    headers = headers || {};
    headers['Authorization'] = 'Bearer ' + accessToken;
    if (process && process.release && process.release.name === 'node') {
      headers['user-agent'] = 'node.js/' + process.version;
      headers['Accept-Encoding'] = 'gzip';
    }
    var instance = axios.create({
      baseURL: baseURL,
      headers: headers,
      agent: agent,
      paramsSerializer: _qs2.default.stringify
    });
    instance.httpClientParams = httpClientParams;
    instance.cloneWithNewParams = function(newParams) {
      return createHttpClient(axios, (0, _assign2.default)((0, _cloneDeep2.default)(httpClientParams), newParams));
    };
    return instance;
  }
})(require('process'));
