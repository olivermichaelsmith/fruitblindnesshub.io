/* */ 
(function(process) {
  var re = new RegExp(process.env.LEAD_NODE_MAJOR_VERSION + '\.', 'g');
  if (!re.test(process.env.TRAVIS_NODE_VERSION)) {
    process.exit(1);
  }
})(require('process'));
