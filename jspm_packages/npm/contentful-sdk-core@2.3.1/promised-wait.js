/* */ 
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _promise = require("babel-runtime/core-js/promise");

var _promise2 = _interopRequireDefault(_promise);

exports.default = promisedWait;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function promisedWait(ms) {
  return new _promise2.default(function (resolve) {
    setTimeout(resolve, ms || 3000);
  });
}