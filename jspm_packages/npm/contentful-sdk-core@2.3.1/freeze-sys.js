/* */ 
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _freeze = require('babel-runtime/core-js/object/freeze');

var _freeze2 = _interopRequireDefault(_freeze);

exports.default = freezeSys;

var _each = require('lodash/each');

var _each2 = _interopRequireDefault(_each);

var _isPlainObject = require('lodash/isPlainObject');

var _isPlainObject2 = _interopRequireDefault(_isPlainObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function freezeObjectDeep(obj) {
  (0, _each2.default)(obj, function (value, key) {
    if ((0, _isPlainObject2.default)(value)) {
      freezeObjectDeep(value);
    }
  });
  return (0, _freeze2.default)(obj);
}

function freezeSys(obj) {
  freezeObjectDeep(obj.sys);
  return obj;
}