/* */ 
'use strict';
Object.defineProperty(exports, "__esModule", {value: true});
exports.default = wrapHttpClient;
var _reduce = require('lodash/reduce');
var _reduce2 = _interopRequireDefault(_reduce);
var _cloneDeep = require('lodash/cloneDeep');
var _cloneDeep2 = _interopRequireDefault(_cloneDeep);
var _rateLimit = require('./rate-limit');
var _rateLimit2 = _interopRequireDefault(_rateLimit);
var _createBackoff = require('./create-backoff');
var _createBackoff2 = _interopRequireDefault(_createBackoff);
function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {default: obj};
}
function wrapHttpClient(http, _ref) {
  var concurrency = _ref.concurrency;
  var delay = _ref.delay;
  var maxRetries = _ref.maxRetries;
  var retryOnTooManyRequests = _ref.retryOnTooManyRequests;
  return (0, _reduce2.default)(['get', 'post', 'put', 'delete', 'patch', 'head'], function(http, methodName) {
    var httpCall = http[methodName].bind(http);
    if (retryOnTooManyRequests) {
      httpCall = maybeBackoff(httpCall, maxRetries);
    }
    http[methodName] = (0, _rateLimit2.default)(httpCall, concurrency, delay);
    return http;
  }, (0, _cloneDeep2.default)(http));
}
function maybeBackoff(fn, maxRetries) {
  return function httpCall() {
    var self = this;
    self.backoff = self.backoff || (0, _createBackoff2.default)(maxRetries);
    var args = Array.prototype.slice.call(arguments);
    var response = fn.apply(self, args);
    response = response.catch(function(error) {
      if (error.status === 429) {
        return self.backoff(error, function() {
          return httpCall.apply(self, args);
        });
      }
      throw error;
    });
    return response;
  };
}
